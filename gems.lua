-- gems are just collectibles that give you 100 points
Gems = {
	instances = {};
}

-- load the gems image
local gemImage = love.graphics.newImage('images/gem.png')

-- get the distance function
local distance = dist

-- localize the instances
local instances = {}

local gravity = 70		-- falling speed
local attraction = 15	-- attraction factor

function Gems:clear()
	self.instances = {}
	instances = self.instances
end

function Gems:create(x, y)
	table.insert(instances, { x = x, y = y })
end

function Gems:update(dt)
	for i=#instances, 1, -1 do
		local gem = instances[i]
		local dist = distance(gem.x, gem.y, Player.x, Player.y)

		-- go towards the player if we are near the player
		if dist < 25 then
			gem.x = gem.x - (gem.x - Player.x) * attraction * dt
			gem.y = gem.y - (gem.y - Player.y) * attraction * dt

		-- otherwise just fall down
		else
			gem.y = gem.y + gravity*dt
		end

		if dist < 8 then
			table.remove(instances, i)
			Gameplay:incScore(100)
			Sound.play('pickup')
		end
	end
end

function Gems:draw()
	-- the gems sprite is a little bright, so i toned down the colors here.
	love.graphics.setColor(180, 180, 180)

	-- and of course, loop through and draw each gem centered.
	for i=1, #instances do
		local gem = instances[i]
		love.graphics.draw(gemImage, gem.x, gem.y, 0, 1, 1,
			gemImage:getWidth() / 2, gemImage:getHeight() / 2)
	end
end
