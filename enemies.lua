Enemies = {
	instances = {};
}

-- get some functions
local dist = dist
local clamp = clamp

-- load enemy image
local enemyImage = love.graphics.newImage('images/enemy.png')

-- localize enemy instances for convenience
local instances = Enemies.instances

-- enemy types container
local enemyTypes = {
	{	-- just moves down; nothing special
		color = {255, 180, 0};
		update = function (self, dt)
			self.y = self.y + 80*dt
		end;
	};
	{	-- swerving
		color = {255, 100, 100};
		init = function (self)
			self.spawnTime = gameTime
		end;
		update = function (self, dt)
			self.x = self.x + math.sin((gameTime + self.spawnTime)*5)*120*dt
			self.y = self.y + 40*dt
		end;
	};
	{	-- dart (is fast and goes toward the player)
		color = {100, 255, 100};
		update = function (self, dt)
			self.x = self.x - (self.x - Player.x) * dt
			self.y = self.y + 120*dt
		end;
	};
	{	-- teleporter ("jumps" down the screen)
		color = {100, 100, 255};
		init = function (self)
			Timer.add(0.3, function(func)
				if self.remove then return end
				self.y = self.y + 20
				self.x = self.x + math.random(-25, 25)
				Timer.add(0.3, func)
			end)
		end;
	};
	{	-- splitter (comes in as one, spawns two more buddies)
		color = {255, 255, 50};
		yvel = 0;
		init = function (self)
			self.x = math.random(28, screen.width - 28)
			Tween(1, self, { y = math.random(10, screen.height) / 3 }, 'outQuad',
				function ()
					local left = Enemies:add(self, true)
					local right = Enemies:add(self, true)
					Tween(1, left, { x = self.x - 20, y = self.y - 6 }, 'outQuad')
					Tween(1, right, { x = self.x + 20, y = self.y - 6 }, 'outQuad')
					Timer.add(1, function()
						self.moving = true
						left.moving = true
						right.moving = true
					end)
				end)
		end;
		update = function (self, dt)
			if self.moving then
				self.yvel = self.yvel + 100*dt
			end
			self.y = self.y + self.yvel*dt
		end;
	};
}

-- create a timer to spawn new enemies
local spawner = Timer.new()

-- function to add a new enemy to the game
-- also return the instance in case we need to do something w/ it
-- noinit param added to prevent the :init() call
function Enemies:add(attributes, noinit)
	local enemy = {
		color = {255, 255, 255};
		x = math.random(screen.width);
		y = -8;
		health = 5 + math.floor((Gameplay.level - 1)/3);
		hurt = 0;

		init = function() end;
		update = function() end;
	}

	-- bleh, self access
	enemy.maxHealth = enemy.health

	-- copy all of the given attributes to this enemy
	attributes = attributes or
		enemyTypes[math.random(math.min(Gameplay.level, #enemyTypes))]
	
	for k,v in pairs(attributes) do
		enemy[k] = v
	end

	-- initialize the enemy if we haven't set noinit to true
	if not (noinit == true) then
		enemy:init()
	end

	-- don't bother addnig the enemy if it's remove attribute is already there
	if not enemy.remove then
		table.insert(instances, enemy)
	end
	return enemy
end

function Enemies:clear()
	self.instances = {}
	instances = self.instances
end

-- start the enemy spawning timer
function Enemies:start(time)
	self:stop()
	spawner:add(1, function (func)
		self:add()
		spawner:add(math.random()*time, func)
	end)
end

-- stop the spawning timer
function Enemies:stop()
	spawner:clear()
end

function Enemies:update(dt)
	-- update the spawning timer
	spawner:update(dt)

	-- move the enemies
	-- go backwards so we can remove enemies
	for i=#instances, 1, -1 do
		local enemy = instances[i]

		enemy:update(dt)
		enemy.x = clamp(0, enemy.x, screen.width)
		enemy.hurt = enemy.hurt - dt

		if enemy.y > screen.height + 10 then
			enemy.remove = true
		end

		if enemy.health <= 0 then
			enemy.remove = true
			enemy.dead = true
			Particles:explode(enemy.x, enemy.y, enemy.color)
			Sound.play('explosion')
			if math.random() > 0.9 then
				Gems:create(enemy.x, enemy.y)
			end
		end

		if enemy.remove then
			table.remove(instances, i)
		end
	end

	-- check the bullets against enemy instances
	-- distance checking because enemy sprites are circle-ish
	local bullets = Player.bullets
	for b=#bullets, 1, -1 do
		local bullet = bullets[b]
		for e=#instances, 1, -1 do
			local enemy = instances[e]
			if dist(bullet.x, bullet.y, enemy.x, enemy.y) < 8 then
				enemy.health = enemy.health - 1
				enemy.hurt = 0.05
				Gameplay:incScore(1)
				Particles:explode(bullet.x, bullet.y, enemy.color, math.random(3))

				if enemy.health <= 0 then
					ScreenShake:trigger(5)
					Gameplay:incScore(10)
					Gameplay.exp = Gameplay.exp + 1
				end

				table.remove(bullets, b)

				-- the bullet's dead and can't hit anything else,
				-- so break out of the loop
				break
			end
		end
	end
end

function Enemies:draw()
	-- draw the enemies
	local w,h = enemyImage:getWidth(), enemyImage:getHeight()
	for i=1, #instances do
		local enemy = instances[i]

		-- set the color to the enemie's color
		love.graphics.setColor(enemy.color)

		-- reset the color to white if the enemy is currently being hit
		if enemy.hurt > 0 then
			love.graphics.setColor(255, 255, 255)
		end

		-- draw the enemy
		love.graphics.draw(enemyImage,
			enemy.x - w/2,
			enemy.y - h/2)

		-- lifebar (only if enemy is damaged)
		if enemy.health < enemy.maxHealth then
			local barWidth = enemy.health / enemy.maxHealth * w
			local barHeight = 2
			love.graphics.setColor(0, 255, 0, 120)
			love.graphics.rectangle('fill',
				enemy.x - barWidth/2, enemy.y - 10,
				barWidth, barHeight)
		end
	end
end
