-- a nice "fade" API to easily switch gamestates with a nice fade effect
Fade = {
	opacity = 0;
	fading = false;
}

function Fade:switch(state, time)
	-- "time" by default is 1 second for fading
	time = time or 1

	-- here we let the game know we're fading
	self.fading = true

	-- start a tween, and set our opacity to 1 in the amount of time given
	-- then when the tween ends, tell the game that we're not fading,
	-- switch to the next gamestate, and fade out.
	Tween(time, self, { opacity = 1 }, nil, function()
		self.fading = false
		Gamestate.switch(state)
		Tween(time, self, { opacity = 0 })
	end)
end

function Fade:draw()
	-- fill the screen with a black rectangle, 
	-- transparent depending on our opacity
	local w,h = love.graphics.getMode()
	love.graphics.setColor(0, 0, 0, 255 * self.opacity)
	love.graphics.rectangle('fill', 0, 0, w, h)
end
