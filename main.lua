function love.load()
	-- set a random seed so there's a different arrangement of enemies
	-- each time
	math.randomseed(os.time())
	math.random() math.random()

	-- make it so that images scaled up aren't blurry
	love.graphics.setDefaultImageFilter('linear','nearest')

	-- convenient access to "real" screen width and height
	screen = { scale = 3 }
	screen.width = love.graphics.getWidth() / screen.scale
	screen.height = love.graphics.getHeight() / screen.scale

	-- set some variables we use for miscellaneous purposes
	gameTime = 0
	gamePaused = false
	fontPath = 'fonts/Beeb Mode One.ttf'

	-- libraries
	Gamestate = require 'hump.gamestate'
	Timer = require 'hump.timer'
	Tween = require 'tween'
	require 'sound'
	require 'extra'

	-- super objects and utilities
	require 'player'
	require 'enemies'
	require 'laser'
	require 'gems'
	require 'stars'
	require 'screenshake'
	require 'particles'
	require 'fade'

	-- game states
	require 'title'
	require 'gameplay'
	require 'gameover'

	-- load 'dem sounds
	Sound.load('sounds')
	local music = love.audio.newSource('Blitz Lunar - You Show.mp3')

	-- we need to handle our states manually.
	-- Gamestate.registerEvents()
	Gamestate.switch(Title)
	
	-- play the music, looping
	-- it's too loud on it's own, so half the volume.
	music:setVolume(0.5)
	music:setLooping(true)
	music:play()
end

function love.update(dt)
	-- update the screenshaking variables, independent of pausing/focus
	ScreenShake:update(dt)

	-- stop the entire game; don't do anything while paused
	if gamePaused then return end
	if not love.graphics.hasFocus() then return end

	-- increment the time the game has been running
	gameTime = gameTime + dt

	-- THIS IS SO HELPFUL
	Timer.update(dt)
	Tween.update(dt)

	-- more gamestate-independent stuff
	Stars:update(dt)
	Particles:update(dt)

	-- aaaaand finally, update the current gamestate
	Gamestate.update(dt)
end

function love.keypressed(k)
	-- we wouldn't want multiple fades happening at once, 
	-- so only send keypresses if we aren't fading
	if not Fade.fading then
		Gamestate.keypressed(k)
	end
end

function love.draw()
	-- shake 'dat screen yo
	ScreenShake:draw()

	-- we have to draw the stars and particles with the screen scaled
	love.graphics.push()
	love.graphics.scale(screen.scale)
	Stars:draw()
	Particles:draw()
	love.graphics.pop()

	-- and everything else without (gamestates do scaling on their own if needed)
	Gamestate.draw()
	Fade:draw()
end
