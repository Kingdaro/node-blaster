-- simple sound manager
Sound = {}

-- our private table of sound sources
local sources = {}

-- load all of the sounds from a specified directory
function Sound.load(dir)
	local files = love.filesystem.enumerate(dir)
	for i=1, #files do
		local file = files[i]
		local filename = file:match('(.+)%..-$')
		
		-- record the sound by name, not by full filename
		-- debatable of how good of an idea this is
		sources[filename] = love.audio.newSource(dir..'/'..file)
	end
end

-- play a sound by name
-- e.g. the sound 'explosion.ogg' would be played with "Sound.play('explosion')"
function Sound.play(name)
	local sound = sources[name]

	-- stopping before playing because of a weird glitch thing
	sound:stop()
	sound:play()
end
