-- those stars in the background
Stars = {
	instances = {}
}

-- "tick" is basically a "manual timer", i suppose
-- it's decremented every update and reset when it gets to 0.
local tick = 0

-- and of course, localize all of our stars (instances)
local instances = Stars.instances

function Stars:update(dt)
	if tick <= 0 then
		-- add a new star once the tick gets to 0
		-- speed is dependent on the game's level
		table.insert(instances, {
			x = math.random(0, screen.width);
			y = 0;
			speed = 3 + Gameplay.level*2;
			size = math.random(3, 6);
		})

		-- add to the tick so we can add another star 0.05 seconds later
		tick = tick + 0.05
	end
	tick = tick - dt

	-- move all of the stars down, adding to their y depending on their size
	for i=#instances, 1, -1 do
		local star = instances[i]
		star.y = star.y + star.speed*star.size*dt
		if star.y > screen.height then
			table.remove(instances, i)
		end
	end
end

-- point-draw all of the stars, like the particles
function Stars:draw()
	love.graphics.setColor(255, 255, 255)
	love.graphics.setPointStyle('rough')
	for i=1, #instances do
		local star = instances[i]
		love.graphics.setPointSize(star.size)
		love.graphics.point(star.x, star.y)
	end
end
