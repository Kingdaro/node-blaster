-- title screen
Title = {}

-- title
local titleFont = love.graphics.newFont(fontPath, 64)
local titleText = 'NODE BLASTER'

-- instructions
local subFont = love.graphics.newFont(fontPath, 36)
local subText = 'ARROWS TO MOVE\nZ OR X TO SHOOT\nESC TO PAUSE'

-- gotta give the artist credit, right? :D
local infoFont = love.graphics.newFont(fontPath, 18)
local infoText = 'MUSIC: BLIZ LUNAR - YOU SHOW'

-- not sure why this is here...
function Title:enter()
end

-- allow exiting on escape
-- go to the game on any other key
function Title:keypressed(k)
	if k == 'escape' then
		love.event.quit()
	else
		Fade:switch(Gameplay)
	end
end

function Title:draw()
	local w,h = love.graphics.getMode()

	love.graphics.push()

	love.graphics.setColor(255, 255, 255)
	love.graphics.translate(0, math.sin(gameTime*2)*10)

	love.graphics.setFont(titleFont)
	love.graphics.printf(titleText, 0, 100, w, 'center')

	love.graphics.setFont(subFont)
	love.graphics.printf(subText, 0, 350, w, 'center')

	love.graphics.pop()

	-- draw the artist credit so it fades away after a second
	if gameTime < 2 then
		love.graphics.setColor(255, 255, 255)
		if gameTime > 1 then
			love.graphics.setColor(255, 255, 255, 255 * (1 - gameTime))
		end
		love.graphics.setFont(infoFont)
		love.graphics.printf(infoText, 0,
			h - infoFont:getHeight(infoText) - 4, w, 'center')
	end
end
