-- just some simple particles to give the game "life"
Particles = {
	instances = {};
}

-- localizing instances
local instances = Particles.instances

-- some general values for how particles behave
local count = 10		-- the default number of particles per explosion
local spread = 8		-- the max distance that the partcles will start from the origin
local velocity = 200	-- the speed the particles move from the origin
local gravity = 500		-- how fast the particles fall down

-- shorthand for making random integers from -1 to 1
local function rand()
	return math.random()*2 - 1
end

function Particles:explode(x, y, color, partNum)
	-- this creates a new table for the particle's color, instead of using the one given
	-- it sets it's __index or "lookup" to the given color, or the default white
	-- if I didn't do this, our given table could be, say, an enemy's color
	-- when this particle fades out, so would the enemy
	-- so yeah, it needs to be a separate, new table object referencing the old
	color = setmetatable({}, { __index = color or {255, 255, 255} })

	for i=1, partNum or count do
		local part = {
			x = x + rand()*spread;
			y = y + rand()*spread;
			xvel = rand()*velocity;
			yvel = rand()*velocity;
			color = color;
			life = 1;
		}
		table.insert(instances, part)
	end
end

function Particles:update(dt)
	-- go through all of the particles
	for i=#instances, 1, -1 do
		local part = instances[i]

		-- apply velocity/gravity and life deduction
		part.x = part.x + part.xvel*dt
		part.y = part.y + part.yvel*dt
		part.yvel = part.yvel + gravity*dt
		part.life = part.life - dt*2

		-- also interpolate the alpha (fourth value in color) with life
		-- so particles "fade out"
		part.color[4] = part.life / 1 * 255

		-- remove any particles below the screen
		if part.y >= screen.height or part.life < 0 then
			table.remove(instances, i)
		end
	end
end

function Particles:draw()
	-- draw all particles using love's built-in point drawer :D
	love.graphics.setPoint(8, 'rough')
	for i=1, #instances do
		local part = instances[i]

		love.graphics.setColor(part.color)
		love.graphics.point(part.x, part.y)
	end
end
