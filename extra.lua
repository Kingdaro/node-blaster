-- clamp function (for bounding stuffs)
function clamp(min, n, max)
	return n < min and min or n > max and max or n
end

-- distance function to check for collisions
function dist(x1, y1, x2, y2)
	return ((x1 - x2)^2 + (y1 - y2)^2) ^ 0.5
end
