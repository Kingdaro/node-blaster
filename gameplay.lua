-- the main game state that calls all the functions and manages leveling/difficulty
Gameplay = {
	level = 1;
	exp = 0;
	required = 0;
	score = 0;
}

-- make fonts for the HUD and paused screen
local scoreFont = love.graphics.newFont(fontPath, 24)
local pausedFont = love.graphics.newFont(fontPath, 36)

-- define numbers for the drawing function to draw effects based off of
-- setting effects.pain to 1 shows the red flashing background when getting hurt
local effects = {
	score = 0;
	leveling = 0;
	pain = 0;
}

function Gameplay:enter()
	-- reset/clear everything, and start the game
	Player:reset()
	Enemies:clear()
	Gems:clear()
	Enemies:start(5)

	-- reset all of the leveling parameters and the score
	self.level = 1
	self.exp = 0
	self.required = 5
	self.score = 0

	-- add a timer for the laser to start seeking (only after level 5)
	Timer.add(math.random(5, 30), function(func)
		if self.level > 5 then
			Laser:seek()
		end
		Timer.add(math.random(10, 30), func)
	end)
end

function Gameplay:update(dt)
	-- update ALL OF THE THINGS
	Player:update(dt)
	Enemies:update(dt)
	Laser:update(dt)
	Gems:update(dt)

	-- the .exp variable increments whenever we kill something
	-- the .required variable is how much .exp we need to get to go to the next level
	if self.exp >= self.required then
		-- we subtract .required from .exp (to account for overflows)
		self.exp = self.exp - self.required

		-- add 3 to required .exp to level
		self.required = math.ceil(self.required + 3)

		-- add to the level
		self.level = self.level + 1

		-- every 3 levels (level 4, 7, 10, 14, etc.) upgrade the player's gun
		if self.level % 3 == 1 then
			Player.shots = Player.shots + 1
		end

		-- restart the enemy timer, and have enemies spawn at a rate
		-- based on our level
		Enemies:start(5 / (self.level * 0.7))

		-- create the leveling background purple flash
		effects.leveling = 1
		Tween(0.3, effects, { leveling = 0 }, 'outQuad')

		-- make the screen shake a little bit
		ScreenShake:trigger(3)
	end
end

function Gameplay:keypressed(k)
	-- pause/unpause the game on escape or p press
	if k == 'p' or k == 'escape' then
		gamePaused = not gamePaused
	end

	-- only give keypressed events if not paused
	if not gamePaused then
		Player:keypressed(k)
	end
end

function Gameplay:draw()
	local w,h = love.graphics.getMode()

	-- draw a purple background for the leveling effect
	love.graphics.setColor(255, 0, 255, effects.leveling*100)
	love.graphics.rectangle('fill', 0, 0, w, h)

	-- draw a red one for the pain effect
	love.graphics.setColor(255, 0, 0, effects.pain*100)
	love.graphics.rectangle('fill', 0, 0, w, h)

	love.graphics.push()

	-- scale the screen
	love.graphics.scale(screen.scale)

	-- draw the stuffs
	Gems:draw()
	Player:draw()
	Enemies:draw()
	Laser:draw()

	local barHeight = 2
	local ratio

	-- exp bar
	--ratio = self.exp / self.required
	--love.graphics.setColor(255, 255 - ratio*255, 255)
	--love.graphics.rectangle('fill', 0, screen.height - barHeight*2,
	--	screen.width * ratio, barHeight)

	-- health bar
	ratio = Player.health / 100
	love.graphics.setColor(255 - ratio*255, ratio*255, 0)
	love.graphics.rectangle('fill', 0, screen.height - barHeight,
		screen.width * ratio, barHeight)

	love.graphics.pop()

	-- level and score display
	love.graphics.setColor(255, 255, 255)
	love.graphics.setFont(scoreFont)
	love.graphics.printf('LEVEL ' .. self.level, 10, 10 + effects.leveling * 8, w)
	love.graphics.printf('SCORE ' .. self.score, -10, 10 + effects.score * 8, w, 'right')

	-- pause overlay
	if gamePaused then
		local text = 'PAUSED'
		local height = pausedFont:getHeight(text)

		love.graphics.setColor(0, 0, 0, 80)
		love.graphics.rectangle('fill', 0, 0, w, h)

		love.graphics.setColor(255, 255, 255)
		love.graphics.setFont(pausedFont)
		love.graphics.printf(text, 0, h/2 - height/2, w, 'center')
	end
end

function Gameplay:incScore(n)
	-- increment the score, also create that "bounce" effect when score is added
	self.score = self.score + n
	effects.score = 1
	Tween(0.1, effects, { score = 0 }, 'outQuad')
end

function Gameplay:painEffect()
	-- create the red background that flashes when you get hurt
	effects.pain = 1
	Tween(0.3, effects, { pain = 0 }, 'outQuad')
end

function Gameplay:leave()
	-- clean up!
	Enemies:stop()
	self.level = 1
	Timer.clear()
	gamePaused = false
end
