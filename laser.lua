-- the big blue powerful laser that tries to kill you
Laser = {}

-- loading images for the target and the laser beam
local targetImage = love.graphics.newImage('images/target.png')
local beamImage = love.graphics.newImage('images/laser.png')

-- define object for the target; the little white cursor that follows you around
-- when seeking is true
local target = { x = 0, y = 0, seeking = false, opacity = 1 }

-- the beam, the actual thing that destroys everything
local beam = { x = 0, y = 0, width = 0 }

-- the laser image by itself is not big enough to cover the screen top-to-bottom
-- so we use a spritebatch and layer the entire thing
-- for however many times we need to
local laserBatch = love.graphics.newSpriteBatch(beamImage)

-- this loop starts at 0, ends at the height of the screen,
-- and increments by the height of the image,
-- therefore filling the spritebatch from top to bottom with images
-- that will encompass the height of the entire screen.
for y=0, screen.height + beamImage:getHeight(), beamImage:getHeight() do
	laserBatch:add(0, y)
end

-- this function is not meant to be called explicitly
-- it is used as a "stencil", which is used in love2d
-- to limit drawing operations to a specific area.
-- we use this stencil to "shorten" the laser when it dies down.
local function laserStencil()
	local iwidth = beamImage:getWidth()
	love.graphics.rectangle('fill',
		beam.x + (1 - beam.width) * iwidth / 2 - iwidth / 2, 0,
		beam.width * iwidth, screen.height)
end

-- to reset the laser so it doesn't fire when we resume the game again.
function Laser:reset()
	target = { x = 0, y = 0, seeking = false, opacity = 1 }
	beam = { x = 0, y = 0, width = 0 }
end

-- this function basically starts the seeking process
-- the target comes onto the screen (starting randomly from the left or right)
-- and attempts to follow the player
-- it then fades for 1 second, and at the same time, sets a timer to fire
-- after 1.5 seconds, so the laser fires 0.5 seconds after the target
-- has completely disappeared.
function Laser:seek()
	target.x = math.random() > 0.5 and -8 or screen.width + 8
	target.y = Player.y
	target.opacity = 1
	target.seeking = true

	Timer.add(3, function ()
		Tween(1, target, { opacity = 0 }, nil, function ()
			target.seeking = false
		end)
		Timer.add(1.5, function()
			self:fire(target.x)
		end)
	end)
end

-- sets the beam to full width, then fades it away one second later.
-- not to be called explicitly
function Laser:fire(x)
	beam.x = x
	beam.y = 0
	beam.width = 1

	Timer.add(1, function ()
		Tween(0.2, beam, { width = 0 }, 'outQuad' )
	end)

	Sound.play('laser')
end

function Laser:update(dt)
	-- make the target "smooth" it's position to the player's
	-- this formula is so good
	if target.seeking then
		target.x = target.x - (target.x - Player.x) * dt
		target.y = target.y - (target.y - Player.y) * dt
	end

	-- make the screen shake like a mofo when the beam is being fired
	if beam.width > 0 then
		ScreenShake:trigger(100)
	end

	-- also move the beam down for an illusion of movement
	beam.y = beam.y + 600*dt

	-- here we check to see if the player or enemies are touching the beam
	-- while the beam is being fired
	-- it's as simple as a distance check on the x axis,
	-- as the beam goes from top-to-bottom and has definite width
	if beam.width == 1 then
		-- damage the player, and push it away from the beam.
		if math.abs(Player.destx - beam.x) < 20 then
			Player:damage(beam.x, Player.y, math.random(50, 75))
		end

		-- destroy any enemies within the vicinity
		local enemies = Enemies.instances
		for i=1, #enemies do
			local enemy = enemies[i]
			if math.abs(enemy.x - beam.x) < 20 then
				enemy.health = 0
			end
		end
	end
end

function Laser:draw()
	-- drawing the target
	if target.opacity > 0 then
		-- calculate a nice alpha value
		local alpha = 255
		if target.opacity < 1 then
			alpha = 255 * target.opacity
		end

		-- everything is dependent on opacity but red
		-- so the target turns red as it fades away
		love.graphics.setColor(255, alpha, alpha, alpha)

		love.graphics.draw(targetImage, target.x, target.y, 0, 1, 1,
			targetImage:getWidth() / 2, targetImage:getHeight() / 2)
	end

	-- drawing the laser beam
	if beam.width > 0 then
		local w, h = beamImage:getWidth(), beamImage:getHeight()
		love.graphics.setColor(255, 255, 255)

		love.graphics.setStencil(laserStencil)
		love.graphics.draw(laserBatch, beam.x - w / 2,
			beam.y % h - h)
		love.graphics.setStencil()
	end
end
