GameOver = {}

-- title text that reads "GAME OVER"
-- or "WOW, SERIOUSLY" if your score is 100 or below
local titleText = 'GAME OVER'
local titleFont = love.graphics.newFont(fontPath, 64)

-- the text that would read your score and the high score
local subText = ''
local subFont = love.graphics.newFont(fontPath, 36)

function GameOver:enter()
	-- here we find our current high score
	-- automatically saved in %APPDATA% by default
	-- or ~/.local/share on linux
	local highscore

	-- check if the file "highscore" exists
	if love.filesystem.exists('highscore') then
		-- read that file yo
		local content = love.filesystem.read('highscore')

		-- if the content is a valid number, use it
		-- otherwise, default to 0
		highscore = tonumber(content) or 0
	end

	-- set and write the highscore to our score if we beat it
	-- or if we didn't find a highscore
	if not highscore or Gameplay.score > highscore then
		highscore = Gameplay.score
		love.filesystem.write('highscore', tostring(highscore))
	end

	-- set the text accordingly
	titleText = Gameplay.score > 100 and 'GAME OVER' or 'WOW, SERIOUSLY'
	subText = 'YOUR SCORE\n'..Gameplay.score..'\n\nHIGH SCORE\n'..highscore
end

function GameOver:keypressed()
	-- see fade.lua
	Fade:switch(Title)
end

function GameOver:draw()
	local w,h = love.graphics.getMode()

	-- pushing/popping for the "hover effect" translation
	love.graphics.push()

	love.graphics.setColor(255, 255, 255)
	love.graphics.translate(0, math.sin(gameTime*2)*10)

	love.graphics.setFont(titleFont)
	love.graphics.printf(titleText, 0, 100, w, 'center')

	love.graphics.setFont(subFont)
	love.graphics.printf(subText, 0, 320, w, 'center')

	love.graphics.pop()
end
