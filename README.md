A cute little space shooter thing that I spent a couple of days making.

The objective is to kill basically every enemy that comes down from the top of the screen. Destroy enough enemies, and you can level up. Every three levels, you get an upgrade on your weapon. They occasionally drop gems, which you can pick up for more score. If you get hit too many times by the enemies, you die.

And yeah, watch out for the big laser.
