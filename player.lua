-- get some functions
local dist = dist
local clamp = clamp

-- create player module
Player = {
	x = 0;
	y = 0;
	destx = 0;
	desty = 0;
	speed = 150;
	bullets = {};
	shots = 2;
	health = 100;
	hurt = 0;
	dead = false;
}

-- load the images for the player's ship and the bullets
local shipImage = love.graphics.newImage('images/ship.png')
local bulletImage = love.graphics.newImage('images/bullet.png')

-- localize the bullets table for convenience and speed
local bullets = Player.bullets
local bulletSpeed = 250

-- autofire timer
local autofire = Timer.new()

-- define player controls
local controls = {
	left = {'left', 'a'};
	right = {'right', 'd'};
	up = {'up', 'w'};
	down = {'down', 's'};
	shoot = {'z','x', 'l','k'};
}

-- reset function to reposition the player on a new game
function Player:reset()
	self.destx = screen.width/2
	self.desty = screen.height - 32
	self.x = self.destx
	self.y = self.desty
	self.health = 100
	self.hurt = 0
	self.dead = false
	self.bullets = {}
	self.shots = 2
	bullets = self.bullets
end

-- function to enflict damage to the player
-- x and y given for knockbacks
function Player:damage(x, y, damage)
	if self.dead then return end

	self.health = clamp(0, self.health - damage, 100)
	self.destx = self.destx + (self.x - x)*3
	self.desty = self.desty + (self.y - y)*3
	self.hurt = 1

	Gameplay:painEffect()
	Sound.play('hurt')

	if self.health <= 0 then
		self.dead = true
		Sound.play('explosion')
		Timer.add(1, function()
			Fade:switch(GameOver, 1.5)
		end)
	end
end

-- because there are multiple ways to trigger shooting, drop it in its own function.
function Player:shoot()
	-- offset is how far away the bullets are from the player
	local offset = 12 / (self.shots - 1)
	for i=1, self.shots do
		local x = self.x - 6 + offset * (i - 1)
		table.insert(bullets, {
			x = x;
			y = self.y;
			xvel = self.shots > 2 and (x - self.x)*(self.shots - 1) or 0;
			yvel = -bulletSpeed + math.abs(x - self.x)*self.shots;
		})
	end

	Sound.play('shoot')
end

function Player:update(dt)
	-- go through the bullets table backwards, so we can remove bullets
	for i=#bullets, 1, -1 do
		local bullet = bullets[i]

		-- move all of the bullets
		bullet.x = bullet.x + bullet.xvel*dt
		bullet.y = bullet.y + bullet.yvel*dt

		-- delete any bullets that leave the screen
		if bullet.y < -10 then
			table.remove(bullets, i)
		end
	end

	-- go through the enemies table to check for hits (by distance)
	-- no backwarding needed, because we don't actually remove anything
	local enemies = Enemies.instances
	for i=1, #enemies do
		local enemy = enemies[i]
		if dist(self.x, self.y, enemy.x, enemy.y) < 12 then
			ScreenShake:trigger(8)
			enemy.health = 0
			self:damage(enemy.x, enemy.y, math.random(25, 50))
		end
	end
	
	if self.dead then return end

	-- update the auto fire timer
	autofire:update(dt)

	-- move the player on keypress
	if love.keyboard.isDown(unpack(controls.left)) then
		self.destx = self.destx - self.speed*dt
	end
	if love.keyboard.isDown(unpack(controls.right)) then
		self.destx = self.destx + self.speed*dt
	end
	if love.keyboard.isDown(unpack(controls.up)) then
		self.desty = self.desty - self.speed*dt
	end
	if love.keyboard.isDown(unpack(controls.down)) then
		self.desty = self.desty + self.speed*dt
	end

	-- keep the player within bounds of the screen
	local w, h = shipImage:getWidth(), shipImage:getHeight()
	self.destx = clamp(w/2, self.destx, screen.width - w/2)
	self.desty = clamp(h/2, self.desty, screen.height - h/2)

	-- smooth the actual position to the destination
	self.x = self.x - (self.x - self.destx) * dt * 15
	self.y = self.y - (self.y - self.desty) * dt * 15

	-- slooooowly(?) heal
	if self.hurt < 0 then
		if self.health < 100 then
			self.health = self.health + 20*dt
		elseif self.health > 100 then
			self.health = 100
		end
	end
	self.hurt = self.hurt - dt
end

function Player:keypressed(k)
	if self.dead then return end

	-- shoot on keypress
	for i=1, #controls.shoot do
		if k == controls.shoot[i] then
			self:shoot()
			break
		end
	end

	-- add a new autofire timer
	-- allows slow lazy fire, or quick, powerful mashing
	autofire:clear()
	autofire:addPeriodic(0.11, function()
		if love.keyboard.isDown(unpack(controls.shoot)) then
			self:shoot()
		end
	end) 
end

function Player:draw()
	love.graphics.setColor(255, 255, 255)

	local w, h = bulletImage:getWidth(), bulletImage:getHeight()
	local ox, oy = 0, 0
	if self.hurt > 0 then
		ox = (math.random() * 2 - 1)*5*self.hurt
		oy = (math.random() * 2 - 1)*5*self.hurt
	end
	
	-- draw every bullet, centered
	for i=1, #bullets do
		local bullet = bullets[i]
		love.graphics.draw(bulletImage,
			bullet.x - w / 2,
			bullet.y - h / 2)
	end

	-- only do the stuff below if we aren't dead
	if self.dead then return end

	-- draw the player ship, centered
	love.graphics.draw(shipImage,
		self.x, self.y,
		(self.destx - self.x) / 20,
		1, 1,
		shipImage:getWidth() / 2 + ox,
		shipImage:getHeight() / 2 + oy)
end
